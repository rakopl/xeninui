XeninUI.LanguageAddons = XeninUI.LanguageAddons or {}

if (!file.IsDir("xenin/languages", "DATA")) then
  file.CreateDir("xenin/languages")
end

local LANG = {}
LANG.Languages = {}

AccessorFunc(LANG, "m_url", "URL")
AccessorFunc(LANG, "m_folder", "Folder")
AccessorFunc(LANG, "m_activeLanguage", "ActiveLanguage")

function LANG:SetID(id)
	self.ID = id

	if (!file.IsDir("xenin/languages/" .. id, "DATA")) then
		file.CreateDir("xenin/languages/" .. id)
	end
end

function LANG:GetID()
	return self.ID
end

function LANG:GetFilePath(lang)
	return "xenin/languages/" .. self:GetID() .. "/" .. lang .. ".json"
end

function LANG:Exists(lang)
	return file.Exists(self:GetFilePath(lang), "DATA")
end

function LANG:SetLocalLanguage(lang, tbl)
	self.Languages[lang] = tbl
end

function LANG:Download(lang, overwrite)
	local p = XeninUI.Promises.new()
	if (self:GetLanguage(lang) and !overwrite) then
		return p:resolve(self:GetLanguage(lang))
	end

	local url = self:GetURL() .. "/raw/master/" .. self:GetFolder() .. "/" .. lang .. ".json"

	http.Fetch(url, function(body, size, headers, code)
		-- If GitLab returns this, it's definitely not something we can use
		-- Just assume it doesn't exist or smth
		if (body:sub(1, 15) == "<!DOCTYPE html>") then
			return p:reject(lang .. " language not found")
		end

		local tbl = util.JSONToTable(body)
		if (!tbl) then
			return p:reject("Unable to decode JSON")
		end

		local version = tbl.version
		local writeToFile = true
		if (self:Exists(lang)) then
			local read = file.Read(self:GetFilePath(lang), "DATA")
			if (read and #read > 0) then
				local decode = util.JSONToTable(read)
				if (decode) then
					writeToFile = version > decode.version
				end
			end
		end

		if (writeToFile) then
			file.Write(self:GetFilePath(lang), body)
		end

		self.Languages[lang] = tbl

		p:resolve(tbl, body, headers)
	end, function(err)
		p:reject(err)
	end)

	return p
end

function LANG:GetLanguage(lang)
	return self.Languages[lang]
end

function LANG:GetPhrase(phrase, replacement)
	local activeLang = self:GetActiveLanguage()
	local tbl = self:GetLanguage(activeLang)
	local str, err

	if (!tbl) then
		if (activeLang != "english") then
			tbl = self:GetLanguage("english")
		
			if (tbl) then
				str, err = tbl.phrases[phrase], "Switched to English as the phrase is missing"
			else
				str, err = phrase, "Language not found"
			end
		else
			str, err = phrase, "Language not found"
		end
	end

	if (!str) then
		str = tbl.phrases[phrase]
	
		if (!str) then
			str = phrase, "Can't find the string in the language"
		end
	end

	if (replacement) then
		for i, v in pairs(replacement) do
			str = str:Replace(":" .. i .. ":", v)
		end
	end

	return str
end

function XeninUI:Language(id)
	if (self.LanguageAddons[id]) then
		return self.LanguageAddons[id]
	end

	local tbl = table.Copy(LANG)
	tbl:SetID(id)

	self.LanguageAddons[id] = tbl
	
	return tbl
end