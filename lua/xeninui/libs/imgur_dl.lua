XeninUI.CachedIcons = XeninUI.CachedIcons or{}

if (!file.IsDir("xenin/icons", "DATA")) then
  file.CreateDir("xenin/icons")
end

function XeninUI:GetIcon(id)
	if (self.CachedIcons[id]) then
		return self.CachedIcons[id]
	end

	local read = file.Read("xenin/icons/" .. id:lower() .. ".png")
	if (read) then
		self.CachedIcons[id] = Material("../data/xenin/icons/" .. id:lower() .. ".png", "smooth")
	else
		self.CachedIcons[id] = "Loading"
	end

	http.Fetch("https://i.imgur.com/" .. id .. ".png", function(body, len)
		local str = "xenin/icons/" .. id:lower() .. ".png"
		file.Write(str, body)

		self.CachedIcons[id] = Material("../data/" .. str, "smooth")
	end)
end